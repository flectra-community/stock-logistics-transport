# Copyright 2018 Creu Blanca
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    "name": "Purchase Location address",
    "summary": "Uses the location address on purchases",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/stock-logistics-transport",
    "author": "Creu Blanca, Odoo Community Association (OCA)",
    "category": "Purchases",
    "depends": ["purchase_stock", "stock_location_address"],
    "installable": True,
}
