# Copyright 2018 Creu Blanca
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    "name": "Stock Location address",
    "summary": "Adds an address on locations",
    "version": "2.0.1.0.0",
    "author": "Creu Blanca, " "Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/stock-logistics-transport",
    "category": "Warehouse Management",
    "depends": ["stock"],
    "data": ["views/stock_location_views.xml"],
    "license": "AGPL-3",
    "installable": True,
    "application": False,
}
