# Flectra Community / stock-logistics-transport

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[stock_location_address_purchase](stock_location_address_purchase/) | 2.0.1.0.0| Uses the location address on purchases
[stock_location_address](stock_location_address/) | 2.0.1.0.0| Adds an address on locations
[stock_dock](stock_dock/) | 2.0.1.1.0| Manage the loading docks of your warehouse.
[shipment_advice](shipment_advice/) | 2.0.1.1.0| Manage your (un)loading process through shipment advices.


